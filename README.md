## Ejercicio - Fizzbuzz

`Fizzbuzz` es un juego para enseñarles a los niños divisiones donde van contando en voz alta y dependiendo de las siguientes reglas reemplazan algunos de los números por palabras.

- `"FizzBuzz"` para múltiplos de 3 y 5.
- `"Fizz"` para múltiplos de 3.
- `"Buzz"` para múltiplos de 5.
- El mismo número para cualquier otro valor.


Crea el método `fizzbuzz` que recibe como parámetro dos números enteros (`min` y `max`). Este método debe regresar un `Array` desde `min` hasta `max` sustituyéndolos con las reglas de fizzbuzz.

Recuerda que el resultado de las comparaciones debe ser true.

>Restricción: No usar las estructuras iterativas `for`, `while`, `do..while`, `until`.


```ruby
#Driver code

p fizzbuzz(3, 5) == ["Fizz", 4, "Buzz"]
p fizzbuzz(10, 15) == ["Buzz", 11, "Fizz", 13, 14, "FizzBuzz"]
```
